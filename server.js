const express = require("express");
const mysql = require("mysql");
const cors = require("cors");
const app = express();
const CONFIG = require("./config.js");

const PORT = process.env.PORT || 3000;

app.use(express.json());
app.use(cors());

const routes = require("./backend/routes/routes.js");
routes(app);

if (process.env.NODE_ENV === "production") {
	const path = require("path");

	app.use(express.static(path.resolve(__dirname, "./frontend/build")));
	app.get("*", (request, response) => {
		response.sendFile(
			path.resolve(__dirname, "./frontend", "build", "index.html")
		);
	});
}

app.listen(PORT, function() {
	console.log(`Node listening on port ${PORT}`);
});
