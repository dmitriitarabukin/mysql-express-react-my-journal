import M from "materialize-css";

function alertNotification(text, classes) {
	M.toast({ html: text, classes: classes });
}

export { alertNotification };
