import React, { useState, useEffect } from "react";
import { Redirect, Route } from "react-router-dom";
import { withRouter } from "react-router";
import M from "materialize-css";
import datepicker_config from "./DatepickerConfig";
import "./DatePicker.css";
import axios from "axios";

function DatePicker(props) {
  let [datepicker_instance, setDatepicker] = useState(null);

  useEffect(() => {
    initDatepicker();
  }, []);

  async function initDatepicker() {
    let { data: days } = await getDays();
    
    // TODO: update datepicker events when new day is filled
    datepicker_config.defaultDate = setDefaultDate();
    datepicker_config.events = convertDaysToEvents(days);

    let datepicker = M.Datepicker.init(
      document.querySelector(".datepicker"),
      datepicker_config
    );

    datepicker.options.onSelect = function onDateSelect(date) {
      let year = date.getFullYear(),
        month = date.getMonth() + 1, // + 1 because January starts with 0
        day = date.getDate(),
        full_date = `${year}-${month}-${day}`;

      redirect(full_date);
    };

    setDatepicker(datepicker);
  }

  function setDefaultDate() {
    return new Date(props.history.location.state);
  }

  function convertDaysToEvents(days) {
    return days.map(day => (new Date(day)).toDateString());
  }

  function getDays() {
    return axios.get("/get-days");
  }

  function openCalendar() {
    datepicker_instance.open();
  }

  function redirect(selected_date) {
    props.history.push(`/days/${selected_date}`, selected_date);
  }

  return (
    <div>
      <i className="button-open-calendar material-icons" onClick={openCalendar}>
        event_note
      </i>
      <input type="text" className="datepicker" />
    </div>
  );
}

const DatePickerWithRouter = withRouter(DatePicker);

export default DatePickerWithRouter;
