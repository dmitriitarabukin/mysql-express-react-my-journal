const datepicker_config = {
  format: 'mmmm dd, yyyy',
  defaultDate: null,
  setDefaultDate: true,
  firstDay: 1,
  yearRange: 0,
  autoClose: true,
  events: [],
  i18n: {
    cancel: 'Отмена',
    clear: 'Очистить',
    done: 'Выбрать',
    months: [
      'Январь',
      'Февраль',
      'Март',
      'Апрель',
      'Май',
      'Июнь',
      'Июль',
      'Август',
      'Сентябрь',
      'Октябрь',
      'Ноябрь',
      'Декабрь'        
    ],
    monthsShort: [
      'Янв',
      'Фев',
      'Мар',
      'Апр',
      'Май',
      'Июнь',
      'Июль',
      'Авг',
      'Сен',
      'Окт',
      'Ноя',
      'Дек'
    ],
    weekdays: [
      'Воскресенье',
      'Понедельник',
      'Вторник',
      'Среда',
      'Четверг',
      'Пятница',
      'Суббота'      
    ],
    weekdaysShort: [
      'Вос',
      'Пон',
      'Вт',
      'Ср',
      'Чет',
      'Пят',
      'Суб'
    ],
    weekdaysAbbrev: ['В', 'П', 'В', 'С', 'Ч', 'П', 'С']
  }
};

export default datepicker_config;