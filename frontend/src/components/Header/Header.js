import React from 'react';
import { Link } from 'react-router-dom';
import DatePickerWithRouter from './DatePicker/DatePicker';
import './Header.css';

function Header() {
	return (
  	<nav>
  	  <div className="nav-wrapper">
        <div className="container">
          <div className="row s12">
  	        <Link to={'/'} className='brand-logo'>My Journal</Link>
            <ul id="nav-mobile" className="right">
              <li><Link to={'/days/current-week'}>Записи на текущей неделе</Link></li>
              <li><DatePickerWithRouter /></li>
            </ul>            
          </div>
        </div>
  	  </div>
  	</nav>
	);
}

export default Header;