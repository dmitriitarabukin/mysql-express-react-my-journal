import React from "react";
import { HashRouter as Router, Route, Switch } from "react-router-dom";
import Header from "../Header/Header";
import AllNotes from "../AllNotes/AllNotes";
import CurrentWeek from "../CurrentWeek/CurrentWeek";
import SelectedDay from "../SelectedDay/SelectedDay";
import "./App.css";
import "material-design-icons/iconfont/material-icons.css";
import axios from "axios";

function App() {
  return (
    <Router>
      <div>
        <Header />
        <div className="container">
          <Switch>
            <Route exact path="/:current_page?" component={AllNotes} />            
            <Route exact path="/days/current-week" component={CurrentWeek} />
            <Route path="/days/:date" component={SelectedDay} />
          </Switch>
        </div>
      </div>
    </Router>
  );
}

export default App;
