import React, { useState, useEffect } from "react";
import axios from "axios";
import M from "materialize-css";
import "./NoteRow.css";
import Preloader from "../../Preloaders/Preloader";
import { alertNotification } from '../../../helpers';

function NoteRow(props) {
  let [note_id, setNoteId] = useState(props.id);
  let [is_preloader_active, setPreloader] = useState(false);
  let [is_edit_state, setEditState] = useState(false);
  let [note_text, setNoteText] = useState(props.text);

  useEffect(() => {
    if (is_edit_state) {
      M.textareaAutoResize(document.querySelector(".materialize-textarea"));
    }
  });

  async function deleteNote(id) {
    try {
      setPreloader(true);
      let result = await axios.delete(`/notes/${id}`);
      setNoteId(null);
      setPreloader(false);
      alertNotification(result.data, 'styled-toast');
      // TODO: remove day from db if there is no more notes
    } catch (err) {
      console.log(err);
      setPreloader(false);
    }
  }

  async function updateNote(id, text) {
    try {
      setPreloader(true);
      let result = await axios.put(`/notes/${id}`, {
        text: text
      });
      setPreloader(false);
      setEditState(false);
      alertNotification(result.data, 'styled-toast');
    } catch (err) {
      setEditState(false);
      console.log(err);
      setPreloader(false);
    }
  }

  if (note_id) {
    return (
      <li className="collection-item">
        {is_edit_state ? (
          <div className="input-field">
            <textarea
              onChange={event => setNoteText(event.target.value)}
              defaultValue={note_text}
              className={
                "materialize-textarea " + (is_preloader_active ? "blurred" : "")
              }
            />
          </div>
        ) : (
          <span className={is_preloader_active ? "blurred" : ""}>
            {note_text}
          </span>
        )}
        {is_edit_state ? (
          <div className="collection-item-buttons-wrap">
            <a
              onClick={() => updateNote(note_id, note_text)}
              className="btn-floating waves-effect waves-light green button-apply-edit-note"
            >
              <i className="material-icons">check</i>
            </a>
            <a
              onClick={() => setEditState(false)}
              className="btn-floating waves-effect waves-light red button-cancel-edit-note"
            >
              <i className="material-icons">clear</i>
            </a>
          </div>
        ) : (
          <div className="collection-item-buttons-wrap">
            <a
              onClick={() => setEditState(true)}
              className="btn-floating waves-effect waves-light blue button-edit-note"
            >
              <i className="material-icons">edit</i>
            </a>
            <a
              onClick={() => deleteNote(note_id)}
              className="btn-floating waves-effect waves-light red button-remove-note"
            >
              <i className="material-icons">remove</i>
            </a>
          </div>
        )}
        {is_preloader_active && <Preloader />}
      </li>
    );
  }

  return null;
}

export default NoteRow;
