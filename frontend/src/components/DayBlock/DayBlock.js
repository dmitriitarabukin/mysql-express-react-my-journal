import React, { useState, useEffect, useRef } from "react";
import NoteRow from "./NoteRow/NoteRow";
import "./DayBlock.css";
import axios from "axios";
import { alertNotification } from "../../helpers";

function DayBlock(props) {
  let [notes, setNotes] = useState(props.notes);
  let [date, setDate] = useState(props.date);
  let [is_add_note_state, setAddNoteState] = useState(false);
  let [new_note_text, setNewNoteText] = useState("");

  useEffect(() => {
    setNotes(props.notes);
    setDate(props.date);
  }, [props.notes]);

  async function addNote(date, text) {
    try {
      let result = await axios.post("/notes", {
        date: date,
        text: text
      });
      setNotes([...notes, ...result.data]);
      setAddNoteState(false);
      alertNotification('Запись успешно добавлена.', "styled-toast");
      // TODO: add date to datepicker event option if the new day is filled
    } catch (error) {
      setAddNoteState(false);
      console.error(error);
    }
  }

  function handleNewNoteChange(event) {
    setNewNoteText(event.target.value);
  }

  // TODO: refactor duplicate function
  function formatDate(date_string, is_for_db) {
    if (is_for_db) {
      let date = new Date(date_string),
          year = date.getFullYear(),
          month = date.getMonth() + 1,
          day = date.getDate();

      return `${year}-${month}-${day}`;
    } else {
      return new Date(date_string).toLocaleDateString("default", {
        year: "numeric",
        month: "long",
        day: "numeric"
      });
    }
  }

  return (
    <ul className="collection with-header">
      <li className="collection-header">
        <h4>
          <strong>
            {formatDate(date, false)} {!notes.length && "(записей нет)"}
          </strong>
        </h4>
        <a
          onClick={() => setAddNoteState(true)}
          className="btn-floating btn-large waves-effect waves-light blue button-add-note-state"
        >
          <i className="material-icons">add</i>
        </a>
      </li>
      {notes.map(note => (
        <NoteRow key={note.id} id={note.id} text={note.text} />
      ))}
      {is_add_note_state && (
        <li className="collection-item">
          <div className="input-field">
            <textarea
              className="materialize-textarea"
              onChange={handleNewNoteChange}
            />
          </div>
          <div className="add-note-buttons-wrap">
            <a
              onClick={() => addNote(formatDate(date, true), new_note_text)}
              className="btn-floating waves-effect waves-light green"
            >
              <i className="material-icons">check</i>
            </a>
            <a
              onClick={() => setAddNoteState(false)}
              className="btn-floating waves-effect waves-light red"
            >
              <i className="material-icons">clear</i>
            </a>
          </div>
        </li>
      )}
    </ul>
  );
}

export default DayBlock;
