import React, { useState, useEffect } from "react";
import axios from "axios";
import DayBlock from "../DayBlock/DayBlock";

function CurrentWeek() {
	let [days, setDays] = useState([]);

	useEffect(() => {
		getCurrentWeekNotes();

		async function getCurrentWeekNotes() {
			try {
				let days = await axios.get("/notes/current-week");
				setDays(days.data);
			} catch (error) {
				console.error(error);
			}
		}
	}, []);

	if (days.length) {
		return (
			<div>
				<h3>Записи на текущей неделе:</h3>
				{days.map((day, index) => {
					return (
						<DayBlock
							key={day.id}
							id={day.id}
							date={day.date}
							notes={day.notes}
						/>
					);
				})}
			</div>
		);
	}

	return <h3>На текущей неделе записей нет</h3>;
}

export default CurrentWeek;
