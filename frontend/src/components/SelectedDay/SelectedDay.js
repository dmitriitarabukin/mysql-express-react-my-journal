import React from "react";
import axios from "axios";
import DayBlock from "../DayBlock/DayBlock";

// Using hooks fails while updating url. Used class component instead.
class SelectedDay extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      date: "",
      notes: []
    };
  }

  async getSelectedDayNotes() {
    let date =
      this.props.history.location.state || this.props.match.params.date;
    let response = await axios.get(`/notes/select/${date}`);

    if (response.data.length) {
      this.setState({
        date: date,
        notes: response.data
      });
    } else {
      this.setState({
        date: date,
        notes: []
      });
    }
  }

  componentDidMount() {
    this.getSelectedDayNotes();
  }

  componentDidUpdate(prev_props, prev_state) {
    if (this.props.match.url !== prev_props.match.url) {
      this.getSelectedDayNotes();
    }
  }

  render() {
    let { date, notes } = this.state;

    if (date && notes) {
      let id;

      if (notes.length > 0) {
        id = notes[0].day_id;
      }

      return <DayBlock date={date} id={id} notes={notes} />;
    }

    return <div>Загрузка...</div>;
  }
}

export default SelectedDay;
