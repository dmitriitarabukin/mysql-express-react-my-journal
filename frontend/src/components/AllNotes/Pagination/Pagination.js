import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import M from "materialize-css";
import "./Pagination.css";

function Pagination({ count, current_page, days_per_page, chooseDaysPerPage }) {
	let days_per_page_select = null;

	count = Number(count);
	current_page = Number(current_page);

	useEffect(() => {
		initializeSelect();
	}, []);

	// TODO: make items as separate components
	function paginationItem(index) {
		return (
			<li key={index} className={index === current_page ? "active" : ""}>
				<Link to={{ pathname: index }}>{index}</Link>
			</li>
		);
	}

	function paginationLeft() {
		return (
			<li
				key="0"
				className={"waves-effect " + (current_page === 1 ? "disabled" : "")}
			>
				<Link to={current_page === 1 ? "#" : String(current_page - 1)}>
					<i className="material-icons">chevron_left</i>
				</Link>
			</li>
		);
	}

	function paginationRight() {
		return (
			<li
				key={count + 1}
				className={"waves-effect " + (current_page === count ? "disabled" : "")}
			>
				<Link to={current_page === count ? "#" : String(current_page + 1)}>
					<i className="material-icons">chevron_right</i>
				</Link>
			</li>
		);
	}

	function paginationDots(index) {
		return <li key={index}>...</li>;
	}

	function daysPerPageSelect() {
		return (
			<div className="input-field">
				<select rel="js-days-per-page-select">
					<option value="" disabled selected>
						Дней на странице
					</option>
					<option value="1">1</option>
					<option value="7">7</option>
					<option value="20">20</option>
					<option value="100">100</option>
				</select>
			</div>
		);
	}

	function initializeSelect() {
		days_per_page_select = M.FormSelect.init(
			document.querySelector('[rel="js-days-per-page-select"]')
		);
		days_per_page_select.el.addEventListener("change", chooseDaysPerPage);
	}

	function populatePagination() {
		let pagination_items = [],
			is_left_dots_set = false,
			is_right_dots_set = false;

		pagination_items.push(paginationLeft());

		for (let i = 1; i <= count; i++) {
			if (
				(i >= 1 && i <= 3) ||
				(i >= count - 2 && i <= count) ||
				(i >= current_page - 2 && i <= current_page + 2)
			) {
				pagination_items.push(paginationItem(i));
			} else if (i > 3 && i < current_page - 2 && !is_left_dots_set) {
				pagination_items.push(paginationDots(i));
				is_left_dots_set = true;
			} else if (i > current_page + 2 && i < count - 2 && !is_right_dots_set) {
				pagination_items.push(paginationDots(i));
				is_right_dots_set = true;
			}
		}

		pagination_items.push(paginationRight());

		return pagination_items;
	}

	return (
		<div className="pagination-wrap">
			<ul className="pagination">{populatePagination()}</ul>
			{daysPerPageSelect()}
		</div>
	);
}

export default Pagination;
