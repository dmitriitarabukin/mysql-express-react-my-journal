import React from "react";
import Pagination from "./Pagination/Pagination";
import DayBlock from "../DayBlock/DayBlock";
import axios from "axios";
import { CONFIG } from "../../config";

class AllNotes extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			days: [],
			pages: null,
			days_per_page: CONFIG.days_per_page
		};
		this.chooseDaysPerPage = this.chooseDaysPerPage.bind(this);
	}

	componentDidMount() {
		this.getNumberOfDays();
		this.getDays();
	}

	componentDidUpdate(prev_props, prev_state) {
		if (this.props.match.url !== prev_props.match.url) {
			this.getDays();
		}
		if (this.state.days_per_page !== prev_state.days_per_page) {
			this.getNumberOfDays();
			this.getDays();
		}
	}

	async getNumberOfDays() {
		try {
			let result = await axios.get("/count-days");
			let days_count = result.data[0].count;

			this.setState({
				pages: Math.ceil(
					days_count / this.state.days_per_page || CONFIG.days_per_page
				)
			});
		} catch (error) {
			console.log(error);
		}
	}

	async getDays() {
		try {
			let result = await axios.get(
				`/days/${this.props.match.params.current_page ||
					1}/${this.state.days_per_page}`
			);
			this.setState({
				days: result.data
			});
		} catch (error) {
			console.log(error);
		}
	}

	chooseDaysPerPage(event) {
		let new_days_per_page = event.target.value;
		this.setState({
			days_per_page: new_days_per_page
		});
	}

	render() {
		let { days, pages, days_per_page } = this.state,
			{ current_page } = this.props.match.params;

		if (current_page > pages) {
			this.props.history.push("/");
		}

		if (days.length) {
			return (
				<div>
					<Pagination
						count={pages}
						current_page={current_page || "1"}
						days_per_page={days_per_page}
						chooseDaysPerPage={this.chooseDaysPerPage}
					/>
					{days.map((day, index) => {
						return (
							<DayBlock
								key={day.id}
								id={day.id}
								date={day.date}
								notes={day.notes}
							/>
						);
					})}
				</div>
			);
		}

		return <div>Загрузка...</div>;
	}
}

export default AllNotes;
