const Note = require("../models/Note.js");
const Day = require("../models/Day.js");
const UTILITIES = require("../../utilities.js");
const CONFIG = require("../../config.js");

exports.addNoteController = async (request, response) => {
	try {
		let date = request.body.date,
			text = request.body.text,
			day_id,
			new_note_id,
			new_note;

		if (!date || !text) {
			response
				.status(400)
				.send({ error: true, message: "Необходима дата и/или текст" });
		} else {
			day_id = await Day.doesDayExist(date);
			if (!day_id) day_id = await Note.createDay(date);
			// adds and returns the newly added note
			new_note_id = await Note.addNote(day_id, text);
			new_note = await Note.getNote(new_note_id);
			response.json(new_note);
		}
	} catch (error) {
		response.send(error);
	}
};

exports.getPagedNotesController = async (request, response) => {
	let page = request.params.page,
		days_per_page = Number(request.params.daysPerPage) || CONFIG.days_per_page, // 7 days per page by default
		page_offset,
		notes;

	try {
		if (page == 1) {
			page_offset = 0;
		} else {
			page_offset = (page - 1) * days_per_page;
		}

		notes = await Note.getPagedNotes(days_per_page, page_offset);
		response.json(UTILITIES.restructureNotesData(notes));
	} catch (error) {
		response.send(error);
	}
};

exports.getCurrentWeekNotesController = async (request, response) => {
	let notes;

	try {
		notes = await Note.getCurrentWeekNotes();
		response.json(UTILITIES.restructureNotesData(notes));
	} catch (error) {
		response.send(error);
	}
};

exports.deleteNoteController = async (request, response) => {
	let note_id = request.params.noteId,
		result;

	try {
		result = await Note.deleteNote(note_id);
		if (result.affectedRows > 0) {
			response.send("Запись успешно удалена.");
		} else {
			response.send("Не удалось удалить запись.");
		}
	} catch (error) {
		response.send(error);
	}
};

exports.updateNoteController = async (request, response) => {
	let note_id = request.params.noteId,
		text = request.body.text;

	try {
		await Note.updateNote(note_id, text);
		response.send("Запись успешно изменена.");
	} catch (error) {
		response.send(error);
	}
};

exports.getSelectedDayNotesController = async (request, response) => {
	let year = request.params.year,
		month = request.params.month,
		day = request.params.day,
		date = year + "-" + month + "-" + day,
		day_id,
		notes;

	try {
		day_id = await Day.doesDayExist(date);
		if (!day_id) throw new Error(null);
		notes = await Note.getSelectedDayNotes(day_id);
		response.json(notes);
	} catch (error) {
		response.send(error);
	}
};