const Day = require("../models/Day.js");
const UTILITIES = require("../../utilities.js");

exports.getNumberOfDaysController = async (request, response) => {
	let result;

	try {
		result = await Day.getNumberOfDays();
		response.json(result);
	} catch (error) {
		response.send(error);
	}
};

exports.getDaysController = async (request, response) => {
	let days;

	try {
		days = await Day.getDays();
		response.json(UTILITIES.restructureDaysData(days));
	} catch (error) {
		response.send(error);
	}
};
