let connection = require("../../db.js");

module.exports = {
	addNote: (day_id, text) => {
		return new Promise((resolve, reject) => {
			connection.query(
				"INSERT INTO notes set ?",
				{ text: text, FK_day_id: day_id },
				function(error, result) {
					if (error) {
						reject(error, null);
					}
					resolve(result.insertId);
				}
			);
		});
	},

	getNote: id => {
		return new Promise((resolve, reject) => {
			connection.query(
				`SELECT id, text 
				FROM notes 
				WHERE id = ?`,
				id,
				(error, result) => {
					if (error) {
						reject(error);
					}
					resolve(result);
				}
			);
		});
	},

	createDay: date => {
		return new Promise((resolve, reject) => {
			connection.query(
				"INSERT INTO days (date) VALUES (?)",
				date,
				(error, result) => {
					if (error) {
						reject(error);
					}
					resolve(result.insertId);
				}
			);
		});
	},

	getCurrentWeekNotes: processResult => {
		return new Promise((resolve, reject) => {
			connection.query(
				`SELECT notes.id, notes.text, notes.FK_day_id, days.date 
				FROM days 
				INNER JOIN notes ON notes.FK_day_id = days.id 
				WHERE YEARWEEK(days.date, 1) = YEARWEEK(CURDATE(), 1) 
				ORDER BY days.date ASC`,
				(error, result) => {
					if (error) {
						reject(error);
					}
					resolve(result);
				}
			);
		});
	},

	deleteNote: id => {
		return new Promise((resolve, reject) => {
			connection.query(
				"DELETE FROM notes WHERE id = ?",
				id,
				(error, result) => {
					if (error) {
						reject(error);
					}
					resolve(result);
				}
			);
		});
	},

	updateNote: (id, text) => {
		return new Promise((resolve, reject) => {
			connection.query(
				`UPDATE notes 
				SET text = ? 
				WHERE id = ?`,
				[text, id],
				(error, result) => {
					if (error) {
						reject(error);
					}
					resolve(result);
				}
			);
		});
	},

	getSelectedDayNotes: day_id => {
		return new Promise((resolve, reject) => {
			connection.query(
				`SELECT days.date, notes.text, notes.id, notes.FK_day_id as day_id 
				FROM days 
				INNER JOIN notes ON notes.FK_day_id = ? 
				WHERE days.id = ?`,
				[day_id, day_id],
				(error, result) => {
					if (error) {
						reject(error);
					}
					resolve(result);
				}
			);
		});
	},

	getPagedNotes: (days_per_page, page_offset) => {
		return new Promise((resolve, reject) => {
			connection.query(
				`SELECT * 
				FROM
					(SELECT DISTINCT * 
					FROM
						(SELECT days.id, days.date 
						FROM days 
						INNER JOIN notes 
						ON notes.FK_day_id = days.id
						) days_unique 
					ORDER BY days_unique.date DESC
					LIMIT ? OFFSET ?					
					) days_sub
				INNER JOIN notes 
				ON notes.FK_day_id = days_sub.id
				ORDER BY days_sub.date DESC`,
				[days_per_page, page_offset],
				(error, result) => {
					if (error) {
						reject(error);
					}
					resolve(result);
				}
			);
		});
	}
};
