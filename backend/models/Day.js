let connection = require("../../db.js");

module.exports = {
	getNumberOfDays: () => {
		return new Promise((resolve, reject) => {
			connection.query(
				// only select days having notes
				`SELECT COUNT(days_sub.id) as count 
				FROM 
					(SELECT DISTINCT * 
					FROM 
						(SELECT days.id 
						FROM days 
						INNER JOIN notes ON notes.FK_day_id = days.id
						) days_unique
					) days_sub`,
				(error, result) => {
					if (error) {
						reject(error);
					}
					resolve(result);
				}
			);
		});
	},

	doesDayExist: date => {
		return new Promise((resolve, reject) => {
			connection.query(
				`SELECT id 
				FROM days 
				WHERE date = ?`,
				date,
				(error, rows) => {
					if (error) {
						reject(error);
					} else {
						if (rows && rows.length) {
							resolve(rows[0].id);
						} else {
							resolve(false);
						}
					}
				}
			);
		});
	},

	getDays: () => {
		return new Promise((resolve, reject) => {
			connection.query(
				`SELECT DISTINCT days.date 
				FROM days 
				INNER JOIN notes ON notes.FK_day_id = days.id 
				ORDER BY date ASC`,
				(error, result) => {
					if (error) {
						reject(error);
					}
					resolve(result);
				}
			);
		});
	}
};
