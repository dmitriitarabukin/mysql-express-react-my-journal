const NOTES_CONTROLLER = require("../controllers/notesController.js");
const DAYS_CONTROLLER = require("../controllers/daysController.js");

module.exports = function(app) {
	app.route("/notes/").post(NOTES_CONTROLLER.addNoteController);

	app
		.route("/notes/current-week")
		.get(NOTES_CONTROLLER.getCurrentWeekNotesController);

	app
		.route("/notes/:noteId")
		.put(NOTES_CONTROLLER.updateNoteController)
		.delete(NOTES_CONTROLLER.deleteNoteController);

	app
		.route("/notes/select/:year-:month-:day")
		.get(NOTES_CONTROLLER.getSelectedDayNotesController);

	app
		.route("/days/:page/:daysPerPage*?")
		.get(NOTES_CONTROLLER.getPagedNotesController);

	app.route("/get-days").get(DAYS_CONTROLLER.getDaysController);

	app.route("/count-days").get(DAYS_CONTROLLER.getNumberOfDaysController);
};
