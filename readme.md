# My Journal

## Features

* Add, edit, remove notes from days
* Select specific days using calendar
* Choose current week days

[DEMO](https://whispering-meadow-73126.herokuapp.com)