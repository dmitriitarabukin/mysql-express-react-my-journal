exports.restructureNotesData = notes => {
	let restructured_notes_array = [],
			previous_date = '',
			current_date = '',
			i = 0;	// TODO: Refactor "i"

	notes.forEach(note => {
		current_date = note.date.toString();
		if (previous_date !== current_date) {
			let day_id = note.FK_day_id;
			restructured_notes_array.push({id: day_id, date: current_date, notes: []});
			restructured_notes_array[i].notes.push({id: note.id, text: note.text});
			i += 1;
		} else {
			restructured_notes_array[i - 1].notes.push({id: note.id, text: note.text});
		}

		previous_date = current_date;
	});	

	return restructured_notes_array;
};

exports.restructureDaysData = days =>  {
	return days.map(day => day.date);
};