const mysql = require("mysql");

let db_config;
if (process.env.NODE_ENV === "production") {
	db_config = require("./dbConfigProd.js");
} else {
	db_config = require("./dbConfigDev.js");
}
db_config = { ...db_config, ...{ timezone: "Z" } };

let connection;

function handleDisconnect() {
	connection = mysql.createConnection(db_config);

	connection.connect(function(error) {
		if (error) {
			console.log("error when connecting to db:", error);
			setTimeout(handleDisconnect, 2000);
		}
	});
	connection.on("error", function(error) {
		console.log("db error", error);
		if (error.code === "PROTOCOL_CONNECTION_LOST") {
			handleDisconnect();
		} else {
			throw error;
		}
	});
}

handleDisconnect();

// bad code TODO: configure mysql server wait_timeout
setInterval(function () {
    connection.query('SELECT 1');
}, 5000);

module.exports = connection;