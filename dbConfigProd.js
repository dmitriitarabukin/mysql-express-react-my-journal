const url = require("url");

const db_url = new URL(process.env.CLEARDB_DATABASE_URL);

module.exports = {
	host: db_url.host,
	user: db_url.username,
	password: db_url.password,
	database: db_url.pathname.substring(1)
};
